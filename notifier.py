#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import logging

from pircel import bot

import tornado.gen
import tornado.httpclient
import tornado.web

logger = logging.getLogger(__name__)


def get_commits(changes):
    for change in reversed(changes):
        yield from reversed(change['commits'])


def get_common_info(body):
    who = body['actor']['display_name']
    repo_name = body['repository']['name']
    return who, repo_name


def summarise_comment(comment_text):
    first_line = comment_text.split('\n')[0]
    if len(first_line) > 80:
        first_line = first_line[:80] + '...'
    return first_line


http = tornado.httpclient.AsyncHTTPClient()


class URLShortener:
    def __init__(self, api_key):
        self.url = 'https://www.googleapis.com/urlshortener/v1/url?key={}'.format(api_key)

    @tornado.gen.coroutine
    def shorten(self, url):
        request = tornado.httpclient.HTTPRequest(self.url,
                                                 method='POST',
                                                 body=json.dumps({'longUrl': url}),
                                                 headers={'Content-Type': 'application/json'})
        response = yield http.fetch(request)
        response_body = json.loads(response.body.decode())
        return response_body['id']


class WebhookHandler(tornado.web.RequestHandler):
    def initialize(self, irc_bot, url_shortener):
        self.bot = irc_bot
        self.shortener = url_shortener

    def get(self):
        self.write('Nothing to see here, move along')

    @tornado.gen.coroutine
    def post(self):
        hook_type = self.request.headers['X-Event-Key']
        logger.info('Received hook type "%s"', hook_type)
        logger.debug('Hook body:\n\n%s\n', self.request.body)
        body = json.loads(self.request.body.decode())

        method_name = 'handle_{}'.format(hook_type.replace(':', '_'))
        try:
            handler_method = getattr(self, method_name)
        except AttributeError:
            logger.debug('Unhandled')
        else:
            yield handler_method(body)

    def send_message(self, message):
        for channel in self.bot.args.channel:
            logger.info(message)
            self.bot.server.send_notice(channel, message)

    @tornado.gen.coroutine
    def handle_repo_push(self, body):
        who, repo_name = get_common_info(body)
        commits = list(get_commits(body['push']['changes']))
        commit_count = len(commits)
        s = '' if commit_count == 1 else 's'

        first_message = '{who} has pushed {commit_count} commit{s} to {repo_name}:'.format(**locals())
        self.send_message(first_message)

        for commit in commits:
            summary = commit['message'].strip().split('\n')[0]
            long_link = commit['links']['html']['href']
            link = yield self.shortener.shorten(long_link)

            msg = '{summary} < {link} >'.format(**locals())
            self.send_message(msg)

    @tornado.gen.coroutine
    def handle_issue_created(self, body):
        who, repo_name = get_common_info(body)
        issue = body['issue']
        link = yield self.shortener.shorten(issue['links']['html']['href'])
        msg = ('{who} has submitted a new {issue[kind]} '
               '(#{issue[id]}) to {repo_name}: '
               '{issue[title]} < {link} >'.format(**locals()))
        self.send_message(msg)

    @tornado.gen.coroutine
    def handle_issue_comment_created(self, body):
        who, repo_name = get_common_info(body)
        issue = body['issue']
        comment = body['comment']
        summary = summarise_comment(comment['content']['raw'])
        link = yield self.shortener.shorten(comment['links']['html']['href'])
        msg = ('{who} has commented on {repo_name} issue #{issue[id]} ({issue[title]}): '
               '{summary} < {link} >'.format(**locals()))
        self.send_message(msg)


def add_args(arg_parser):
    arg_parser.add_argument('-k', '--api-key',
                            help='Goo.gl URL shortener API Key')
    return arg_parser


def main():
    irc_bot = bot.IRCBot.from_default_args(nick='bbn', mutate_parser=add_args)
    url_shortener = URLShortener(irc_bot.args.api_key)

    # setup logging
    log_level = logging.DEBUG if irc_bot.args.debug else logging.INFO
    log_date_format = "%Y-%m-%d %H:%M:%S"
    log_format = "%(asctime)s\t%(levelname)s\t%(module)s:%(funcName)s:%(lineno)d\t%(message)s"
    logging.basicConfig(level=log_level, format=log_format, datefmt=log_date_format)
    logging.captureWarnings(True)

    http_listener = tornado.web.Application([
        (r'/', WebhookHandler, dict(irc_bot=irc_bot, url_shortener=url_shortener)),
    ])

    http_listener.listen(9000)

    irc_bot.main()

if __name__ == '__main__':
    main()
