#!/usr/bin/env python
# -*- coding: utf8 -*-
from setuptools import setup

install_requires = [
    'pircel[bot]>=0.3.4',
]

classifiers = [
    'Development Status :: 2 - Pre-Alpha',
    'Topic :: Communications :: Chat :: Internet Relay Chat',
    'License :: OSI Approved :: BSD License',
    'Programming Language :: Python :: 3 :: Only',
]

setup(
    # Metadata
    name='bbn',
    version='0.1.1',
    author='Kit Barnes',
    author_email='kit@ninjalith.com',
    description='',
    url='https://bitbucket.org/KitB/possel/',
    license='BSD',
    keywords='irc quassel',
    classifiers=classifiers,
    zip_safe=False,

    # Non-metadata (mostly)
    packages=[],
    py_modules=['notifier'],
    install_requires=install_requires,
    extras_require={},
    scripts=['bin/bbn'],
    package_data={},
)
